from bs4 import BeautifulSoup
from pathlib import Path
import pandas as pd
import sys
from util import get_content, get_headers, get_title_list, save_df

def save_main_page(src_directory, src_filename, target_directory):
    with open(f"{src_directory}/{src_filename}.html") as fp:
        soup = BeautifulSoup(fp, "html.parser")

    table = soup.select_one('#example')
    save_df(pd.DataFrame(columns = get_headers(table, css_selector='thead th.sorting'), 
                         data = [[x[index].text if index < len(x) - 2
                                  else x[index].a.get('href') 
                                  for index in range(len(x))]
                                  for x in get_content(table, css_selector='tbody > tr', text=False)]),
            f"{target_directory}/{src_filename}.csv")

def save_reports(src_directory, src_filename, target_directory):
    with open(f"{src_directory}/{src_filename}.html") as fp:
        soup = BeautifulSoup(fp, "html.parser")

    Path(f"{target_directory}/{src_filename}").mkdir(parents=True, exist_ok=True)

    table_list = soup.select('html body div.salto.texto-pequenho table.table.table-bordered.table-sm')[1:]
    title_list = get_title_list(soup.select('div.salto.texto-pequenho p>*>*'))
    title_list.insert(0, 'RESUMEN_DE_GASTOS_ELECTORALES')
    # Main table
    save_df(pd.DataFrame(columns=get_headers(table_list[0], css_selector='table.table.table-bordered.table-sm tbody tr td>u>b'), 
                         data=get_content(table_list[0], css_selector='table.table.table-sm.table-bordered tbody tr')[2:-1]), 
            f"{target_directory}/{src_filename}/{title_list.pop(0)}.csv")
    table_list.pop(0)
    for table in table_list:
        data = get_content(table, css_selector='table.table.table-bordered.table-sm tbody tr')[1:]
        df_table = pd.DataFrame(columns=get_headers(table, css_selector='table.table.table-bordered.table-sm tbody tr td.linea_titulo'),
                             data=(data if "TOTAL" not in data[-1][0] else data[:-1]))

        df_table.insert(loc=0, column='CI', value=[src_filename for _ in range(len(df_table))])
        df_table.insert(loc=1, column='NOMBRE', value=[soup.select('tr.alert-dark:nth-child(5) > '\
                                                                   'td:nth-child(1) > '\
                                                                    'b:nth-child(1)')[0].text.split('CANDIDATO',1)[1].strip()
                                                       for _ in range(len(df_table))])

        save_df(df_table, f"{target_directory}/{src_filename}/{title_list.pop(0)}.csv")

def save_csv(src_directory, target_directory):
    if Path(f"{src_directory}").exists():
        Path(f"{target_directory}").mkdir(parents=True, exist_ok=True)
        _dir = Path(f"{src_directory}").glob('*/*')
        filenames = [x for x in _dir if x.is_file()]
        for f in filenames:
            if "rcnd2023-main-page-" in f.stem:
                save_main_page(f.parent, f.stem, f"{target_directory}/{f.parent.stem}")
            else:
                save_reports(f.parent, f.stem, f"{target_directory}/{f.parent.stem}")

def main():
    src_directory = f"{Path.cwd()}/test-data/rcnd2023/src/"
    target_directory = f"{Path.cwd()}/test-data/rcnd2023/target/"

    if len(sys.argv) > 2:
        save_csv(sys.argv[1], sys.argv[2])
    else:
        save_csv(src_directory, target_directory)

if __name__ == '__main__':
    main()