import csv
import pandas as pd
from unidecode import unidecode

def get_headers(table, css_selector, text = True):
    return [x.text.strip() for x in table.select(css_selector)]

def get_content(table, css_selector, text = True):
    return [[y.text if text else y for y in x.select('td')] for x in table.select(css_selector)]

def get_main_info(info_table):
    return pd.DataFrame(columns = get_headers(info_table, css_selector='tr th'), 
                              data = [[x[0] for x in get_content(info_table, css_selector='tr')]])

def save_df(df, path):
    df.to_csv(path, index=False, quotechar='"', quoting=csv.QUOTE_NONNUMERIC)

def replace_str(text, dict):
    for key in dict:
        text = text.replace(key, dict[key])

    return unidecode(text.strip())

def get_title_list(title_list):
    return [replace_str(x.text, {" ": "_", "-": "_", "__": "_", "/": "_", ".": "", "\n": "", ",": ""}) for x in title_list]
