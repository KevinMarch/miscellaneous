from bs4 import BeautifulSoup
from pathlib import Path
import pandas as pd
from collections import OrderedDict
import sys
from util import get_content, get_headers, get_main_info, get_title_list, save_df

def save_main_page(src_directory, src_filename, target_directory):
    with open(f"{src_directory}/{src_filename}.html") as fp:
        soup = BeautifulSoup(fp, "html.parser")

    table = soup.select_one('#example')
    headers = get_headers(table, css_selector='thead th.sorting')[:-2] + ['DIGC Candidato', 'DIEV Candidato', 'DIEV Conyuge']
    raw_data = get_content(table, css_selector='tbody > tr', text=False)
    
    data = []
    for row in raw_data:
        aux_list = []
        for item in row[:-2]:
            aux_list.append(item.text)
        

        candidate_digc, candidate_diev = row[-2].select('a:nth-of-type(1)'), row[-2].select('a:nth-of-type(2)')
        spouse_diev = row[-1].select('a:nth-of-type(1)')

        if candidate_digc: aux_list.append(candidate_digc[0].get('href')) 
        else: aux_list.append('')
        
        if candidate_diev: aux_list.append(candidate_diev[0].get('href')) 
        else: aux_list.append('')

        if spouse_diev: aux_list.append(spouse_diev[0].get('href')) 
        else: aux_list.append('')

        data.append(aux_list)
    
    save_df(pd.DataFrame(columns=headers, data=data), f"{target_directory}/{src_filename}.csv")

def save_spouse_reports(src_directory, src_filename, target_directory):
    with open(f"{src_directory}/{src_filename}.html") as fp:
        soup = BeautifulSoup(fp, "html.parser")

    Path(f"{target_directory}/{src_filename}").mkdir(parents=True, exist_ok=True)
    title_list = list(OrderedDict.fromkeys(get_title_list(soup.select('tr td u b.texto-pequenho'))))
    title_list.insert(0, 'DATOS_PERSONALES_DEL_CÓNYUGE')
    table_list = list(OrderedDict.fromkeys(soup.select('html body table.table.table-sm.table-bordered')))

    save_df(get_main_info(table_list.pop(0)), f"{target_directory}/{src_filename}/{title_list.pop(0)}.csv")

    for table in table_list:
        df_table = pd.DataFrame(columns=get_headers(table, css_selector='table.table.table-sm.table-bordered tbody tr.alert-dark th'),
                             data=get_content(table, css_selector='table.table.table-sm.table-bordered tbody tr')[1:])
        
        df_table.insert(loc=0, column='CI', value=[src_filename[:src_filename.index("_")] for _ in range(len(df_table))])
        df_table.insert(loc=1, column='NOMBRE', value=[f"{soup.select('table.table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2)')[0].text.strip()}" + " "
                                                       f"{soup.select('table.table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2)')[0].text.strip()}"
                                                       for _ in range(len(df_table))])
        save_df(df_table, f"{target_directory}/{src_filename}/{title_list.pop(0)}.csv")


def save_candidate_reports(src_directory, src_filename, target_directory):
    with open(f"{src_directory}/{src_filename}.html") as fp:
        soup = BeautifulSoup(fp, "html.parser")

    Path(f"{target_directory}/{src_filename}").mkdir(parents=True, exist_ok=True)

    title_list = list(OrderedDict.fromkeys(get_title_list(soup.select('tr td u b.texto-pequenho'))))
    title_list.insert(0, 'DATOS_DE_LA_CANDIDATURA_INSCRIPTA')
    title_list.insert(0, 'DATOS_PERSONALES_DEL_CANDIDATO')
    table_list = list(OrderedDict.fromkeys(soup.select('html body table.table.table-sm.table-bordered')))
    
    save_df(get_main_info(table_list.pop(0)), f"{target_directory}/{src_filename}/{title_list.pop(0)}.csv")

    # Two or more candidatures
    if len(table_list) > 5:
        df_tmp_table = get_main_info(table_list.pop(0))
        while len(table_list) > 4:
            df_tmp_table = pd.concat([df_tmp_table, get_main_info(table_list.pop(0))], axis=0)
        
        save_df(df_tmp_table, f"{target_directory}/{src_filename}/{title_list.pop(0)}.csv")
    else:
        save_df(get_main_info(table_list.pop(0)), f"{target_directory}/{src_filename}/{title_list.pop(0)}.csv")
    
    for table in table_list:
        df_table = pd.DataFrame(columns=get_headers(table, css_selector='table.table.table-sm.table-bordered tbody tr.alert-dark th'), 
                                data=get_content(table, css_selector='table.table.table-sm.table-bordered tbody tr')[1:])
        
        df_table.insert(loc=0, column='CI', value=[src_filename[:src_filename.index("_")] for _ in range(len(df_table))])
        df_table.insert(loc=1, column='NOMBRE', value=[f"{soup.select('div.texto-pequenho:nth-child(4) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2)')[0].text.strip()}" + " "
                                                       f"{soup.select('div.texto-pequenho:nth-child(4) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2)')[0].text.strip()}"
                                                for _ in range(len(df_table))])
        save_df(df_table, f"{target_directory}/{src_filename}/{title_list.pop(0)}.csv")


def save_csv(src_directory, target_directory):
    if Path(f"{src_directory}").exists():
        _dir = Path(f"{src_directory}").glob('*/*/*')
        filenames = [x for x in _dir if x.is_file()]
        for f in filenames:
            if "diev-main-page-" in f.stem:
                Path(f"{target_directory}/main-pages/{f.parent.name}").mkdir(parents=True, exist_ok=True)
                save_main_page(f.parent, f.stem, f"{target_directory}/main-pages/{f.parent.name}")
            else:
                Path(f"{target_directory}/reports/{f.parent.name}").mkdir(parents=True, exist_ok=True)
                if "conyuge" in f.stem:
                    save_spouse_reports(f.parent, f.stem, f"{target_directory}/reports/{f.parent.name}")
                elif "candidato" in f.stem:
                    save_candidate_reports(f.parent, f.stem, f"{target_directory}/reports/{f.parent.name}")


def main():
    src_directory = f"{Path.cwd()}/test-data/diev/src/"
    target_directory = f"{Path.cwd()}/test-data/diev/target/"


    if len(sys.argv) == 3:
        save_csv(sys.argv[1], sys.argv[2])
    else:
        save_csv(src_directory, target_directory)

if __name__ == '__main__':
    main()