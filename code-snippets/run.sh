#!/bin/bash

#
# Run any script in background with logging
# Example:
# source run.sh .ve/bin/python -u script.py arg1 arg2 argN
#

echo -e "$(date):\n" >> log.txt &&
nohup /usr/bin/time -f "Execution took %E." $@ >> log.txt &
