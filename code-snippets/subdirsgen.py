import os
import shutil
import sys
import zlib

# https://github.com/open-contracting/kingfisher-collect/blob/main/kingfisher_scrapy/extensions/files_store.py#L87C1-L94C30
def _get_subdirectory(file_name: str) -> str:
    checksum = zlib.adler32(file_name.encode())
    checksum, dir_1 = divmod(checksum, 0x1000)
    dir_2 = checksum % 0x1000
    return '%03X' % dir_1, '%03X' % dir_2

def _subdirsgen(dirpath: str):
    for f in os.scandir(dirpath):
        if f.is_file():
            dir_1, dir_2 = _get_subdirectory(f.name)
            subdir = os.path.join(os.path.dirname(f), dir_1, dir_2)
            os.makedirs(subdir, exist_ok=True)
            shutil.move(os.path.join(os.path.dirname(f), f.name), subdir)

def main(argc: int, argv: [str]) -> int:
    if argc != 2:
        print(f'expected {argv[0]} dirpath')
        return 1

    if not os.path.isdir(argv[1]):
        print(f"{argv[1]} isn't a directory")
        return 1

    _subdirsgen(argv[1])

    return 0

if __name__ == '__main__':
    main(len(sys.argv), sys.argv)
