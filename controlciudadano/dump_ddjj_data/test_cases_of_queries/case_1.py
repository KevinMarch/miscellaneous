import json
import os
import time

from dotenv import load_dotenv
import psycopg2

# Cargar las variables de entorno desde el archivo .env
load_dotenv()

# Obtener los valores de las variables de entorno
dbname = os.getenv("PG_NAME")
user = os.getenv("PG_USER")
port=os.getenv("PG_PORT")
password = os.getenv("PG_PASSWORD")
host = os.getenv("PG_HOST")

# Establecer la conexión a la base de datos
conn = psycopg2.connect(
    dbname=dbname,
    user=user,
    password=password,
    host=host,
    port=port
)
# Crear un cursor para ejecutar consultas
cursor = conn.cursor()
# Verifica si el cursor está conectado
if not cursor.closed:
    print("El cursor está conectado.")
else:
    print("El cursor está cerrado.")

num_rows = 1000
output_folder="data_case_1"
# Crear la carpeta si no existe
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

# Iniciar el temporizador
start_time = time.time()

i=1
j=1

#procesamos los datos
while(i<=10000):
    # Ejecutar una consulta SQL
    cursor.execute(
        f"SELECT * FROM ms_djbr_parser_cc.djbr_extracted_data ded WHERE id >= {i} ORDER BY id asc LIMIT {j * 100}")
    i+=100
    j+=1

# Detener el temporizador
end_time = time.time()

# Calcular la duración de la consulta en segundos
duration = end_time - start_time

# Imprimir el tiempo transcurrido
print(f"La consulta tardó {duration:.2f} segundos")

# Cerrar el cursor y la conexión
cursor.close()
conn.close()