import zlib
from ftplib import FTP
import csv
import os
from dotenv import load_dotenv

def _get_subdirectory(file_name: str) -> str:
    checksum = zlib.adler32(file_name.encode())
    checksum, dir_1 = divmod(checksum, 0x1000)
    dir_2 = checksum % 0x1000
    return '%03X' % dir_1, '%03X' % dir_2

# Load environment variables from the .env file
load_dotenv()
# Get FTP server data from environment variables
ftp_host = os.getenv("FTP_HOST")
ftp_user = os.getenv("FTP_USER")
ftp_password = os.getenv("FTP_PASSWORD")
print(ftp_host, " ", ftp_user)
# Path of the CSV file with file names
csv_path = 'other_assets_list.csv'  # Replace with the actual path

# Local destination directory
local_directory = "dump"  # Replace with the desired local path

# Base directory of the folder
base_dir = "/datasets/assets/paraguay_assets"
# Check if the local directory exists, and if not, create it
if not os.path.exists(local_directory):
    os.makedirs(local_directory)

execution_count = 0
# Connect to the FTP server
with FTP(ftp_host) as ftp:
    ftp.login(ftp_user, ftp_password)

    # Verify that the connection has been established successfully
    if ftp.getwelcome():
        print("FTP connection established successfully:", ftp.getwelcome())

    # Read file names from the CSV file
    with open(csv_path, 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        # Skip the first row containing headers
        next(csv_reader, None)
        file_names = [row[0] for row in csv_reader]

    # Download files to the local directory
    for file_name in file_names:
        dir_1, dir_2 = _get_subdirectory(file_name)
        subdir = os.path.join(base_dir, dir_1, dir_2)
        server_path = subdir
        local_path = os.path.join(local_directory, f"{file_name}.json")  # Append ".json" to the file name

        # Check if the local file already exists before downloading
        if not os.path.exists(local_path):
            with open(local_path, 'wb') as local_file:
                ftp.retrbinary(f'RETR {server_path}/{file_name}.json', local_file.write)
                # Increment the execution count
                execution_count += 1

        # Check if 100 executions have been performed
        if execution_count >= 100:
            break
    print("Files downloaded successfully to the local directory:", local_directory)
