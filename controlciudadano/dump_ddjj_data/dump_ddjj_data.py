import json
import os
import time

from dotenv import load_dotenv
import psycopg2
import os
import shutil
import sys
import zlib

# https://github.com/open-contracting/kingfisher-collect/blob/main/kingfisher_scrapy/extensions/files_store.py#L87C1-L94C30
def _get_subdirectory(file_name: str) -> str:
    checksum = zlib.adler32(file_name.encode())
    checksum, dir_1 = divmod(checksum, 0x1000)
    dir_2 = checksum % 0x1000
    return '%03X' % dir_1, '%03X' % dir_2

# Cargar las variables de entorno desde el archivo .env
load_dotenv()

# Obtener los valores de las variables de entorno
dbname = os.getenv("PG_NAME")
user = os.getenv("PG_USER")
port=os.getenv("PG_PORT")
password = os.getenv("PG_PASSWORD")
host = os.getenv("PG_HOST")

# Establecer la conexión a la base de datos
conn = psycopg2.connect(
    dbname=dbname,
    user=user,
    password=password,
    host=host,
    port=port
)
# Crear un cursor para ejecutar consultas
cursor = conn.cursor()
# Verifica si el cursor está conectado
if not cursor.closed:
    print("El cursor está conectado.")
else:
    print("El cursor está cerrado.")

# Iniciar el temporizador
start_time = time.time()

# Ejecutar una consulta SQL
cursor.execute("SELECT * FROM ms_djbr_parser_cc.djbr_extracted_data")
num_rows = 10

# Detener el temporizador
end_time = time.time()

# Calcular la duración de la consulta en segundos
duration = end_time - start_time

# Imprimir el tiempo transcurrido
print(f"La consulta tardó {duration:.2f} segundos")

output_folder="data"
# Crear la carpeta si no existe
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

#procesamos los datos
while(True):
    # Recuperar varias filas a la vez
    rows = cursor.fetchmany(num_rows)
    print(rows)
    if not rows:
        break
    # Procesar las filas obtenidas
    for row in rows:
        #accedera la columna hash
        hash_value=row[3]
        #accedera la columna json
        json_data=row[2]

        dir_1, dir_2 = _get_subdirectory(hash_value)
        subdir = os.path.join(output_folder, dir_1, dir_2)
        print(subdir)
        os.makedirs(subdir, exist_ok=True)
        # Guardar el contenido en un archivo JSON
        with open(os.path.join(subdir,f'{hash_value}.json'), 'w') as file:
            json.dump(json_data, file, ensure_ascii=False)
            
# Cerrar el cursor y la conexión
cursor.close()
conn.close()