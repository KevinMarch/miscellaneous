#!/bin/bash

#
# control ciudadano py server
# these commands needs to be executed step by step manually inside pure-ftpd container
#

docker exec -ti $(docker ps -aqf "name=pure-ftpd") /bin/sh
pure-pw useradd hackcorruption -f /etc/pure-ftpd/passwd/pureftpd.passwd -m -u 1000 -g 1000 -d /home/ftpuser/data/hackcorruption

#
# Security and auto-mounting notes:
# Append to the fstab file or append to the .netrc doesn't work.
# Because the password contains spaces.
# Using escape characters like \040 ascii code or double quotes "" also doesn't work.
#
#
# touch /home/cdssa/.netrc
# tee -a /home/cdssa/.netrc << END
# machine datapy.ftp.cds.com.py
# login
# password
# END
# chmod 600 /home/cdssa/.netrc
# sudo cp /etc/fstab /etc/fstab.old
# echo 'curlftpfs#datapy.ftp.cds.com.py:/data/hackcorruption /mnt/controlciudadano fuse rw,allow_other,_netdev,user 0 0' | sudo tee -a /etc/fstab
# sudo mount -a
#

# connection
sudo apt install curlftpfs
sudo mkdir /mnt/controlciudadano/
sudo chmod 777 /mnt/controlciudadano/
curlftpfs datapy.ftp.cds.com.py:/data/hackcorruption /mnt/controlciudadano -o user=:""

# folder structure
mkdir /mnt/controlciudadano/hackcorruption
mkdir /mnt/controlciudadano/hackcorruption/datasets
mkdir /mnt/controlciudadano/hackcorruption/datasets/ocds
mkdir /mnt/controlciudadano/hackcorruption/datasets/ocds/paraguay_dncp_records
mkdir /mnt/controlciudadano/hackcorruption/datasets/ocds/panama_dgcp_records
mkdir /mnt/controlciudadano/hackcorruption/datasets/ocds/guatemala_guatecompras_releases
mkdir /mnt/controlciudadano/hackcorruption/datasets/ocds/peru_compras_releases
mkdir /mnt/controlciudadano/hackcorruption/datasets/ocds/peru_osce_api_records

