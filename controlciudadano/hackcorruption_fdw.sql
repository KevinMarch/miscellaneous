--Conexión a Control Ciudadano
--Foreign Data Wrapper
--Crear la extensión 
CREATE EXTENSION postgres_fdw;

-- crer server
CREATE SERVER datapy FOREIGN DATA WRAPPER 
postgres_fdw OPTIONS (host 'datapy.cds.com.py', dbname 'db', port '25432');

--Ver los roles 
SELECT rolname FROM pg_roles; 

-- crear el usuario para el mapping
CREATE USER MAPPING FOR admin_hc
    SERVER datapy OPTIONS (user '', password '');
   
 
--Importar schema ocds
IMPORT FOREIGN SCHEMA ocds
    FROM SERVER datapy
    INTO ocds_cc;
   
--Importar tabla de nómina
IMPORT FOREIGN SCHEMA staging
	limit to (hacienda_funcionarios)
    FROM SERVER datapy
    INTO ocds_cc;

--Importar schema ocds
IMPORT FOREIGN SCHEMA ocds
    FROM SERVER datapy
    INTO ocds_cc;

--Importar schema analisis
IMPORT FOREIGN SCHEMA analisis
    FROM SERVER datapy
    INTO analisis_cc;

--Importar schema analysis
IMPORT FOREIGN SCHEMA analysis
    FROM SERVER datapy
    INTO analysis_cc;

--Importar schema ms_djbr_parser
IMPORT FOREIGN SCHEMA ms_djbr_parser
    FROM SERVER datapy
    INTO ms_djbr_parser_cc;

--Importar schema observatorio
IMPORT FOREIGN SCHEMA observatorio
    FROM SERVER datapy
    INTO observatorio_cc;

--Importar schema ocds_local
IMPORT FOREIGN SCHEMA ocds_local
    FROM SERVER datapy
    INTO ocds_local_cc;

--Importar schema ocds_old
IMPORT FOREIGN SCHEMA ocds_old
    FROM SERVER datapy
    INTO ocds_old_cc;

--Importar schema public
IMPORT FOREIGN SCHEMA public
    FROM SERVER datapy
    INTO public_cc;

--Importar schema staging
IMPORT FOREIGN SCHEMA staging
    FROM SERVER datapy
    INTO staging_cc;

--Importar schema temp_schema_similarities
IMPORT FOREIGN SCHEMA temp_schema_similarities
    FROM SERVER datapy
    INTO temp_schema_similarities_cc;

--Importar schema view_data_collection_3
IMPORT FOREIGN SCHEMA view_data_collection_3
    FROM SERVER datapy
    INTO view_data_collection_3_cc;

--Importar schema view_data_dncp_data
IMPORT FOREIGN SCHEMA view_data_dncp_data
    FROM SERVER datapy
    INTO view_data_dncp_data_cc;

--Importar schema views
IMPORT FOREIGN SCHEMA views
    FROM SERVER datapy
    INTO views_cc;
