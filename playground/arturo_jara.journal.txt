
2023-01-18


# RCND2023:
# Load RCND2023 main pages
LOAD CSV WITH HEADERS FROM 'file:///csv/sinafip-scraper-rcnd2023/rcnd2023-page-2/rcnd2023-main-page-2.csv' AS row
WITH row, split(row.`Cédula`, '-') AS ruc
MERGE (p:Person {id: ruc[0]})
SET p.name = row.`Nombre` + ' ' + row.`Apellido`,
    p.scheme = 'PY-RUC',
    p.metadata_data_source_name = 'sinafip',
    p.metadata_data_source_country ='PY',
    p.metadata_data_source_created_at = CASE WHEN p.metadata_data_source_created_at is NULL THEN datetime() ELSE p.metadata_data_source_created_at END,
    p.metadata_data_source_lastest_update = datetime(),
    p.metadata_data_source_updated_times = CASE WHEN p.metadata_data_source_updated_times IS NULL THEN 1 ELSE p.metadata_data_source_updated_times + 1 END;

# RCND2023:
# Load RCND2023 reports 
LOAD CSV WITH HEADERS FROM 'file:///csv/sinafip-scraper-rcnd2023/rcnd2023-page-2/rcnd2023-main-page-2.csv' AS row
WITH row, split(row.`Cédula`, '-') AS ruc
MERGE (p:Person {id: ruc[0]})
SET p.name = row.`Nombre` + ' ' + row.`Apellido`,
    p.scheme = 'PY-RUC',
    p.metadata_data_source_name = 'sinafip',
    p.metadata_data_source_country ='PY',
    p.metadata_data_source_created_at = CASE WHEN p.metadata_data_source_created_at is NULL THEN datetime() ELSE p.metadata_data_source_created_at END,
    p.metadata_data_source_lastest_update = datetime(),
    p.metadata_data_source_updated_times = CASE WHEN p.metadata_data_source_updated_times IS NULL THEN 1 ELSE p.metadata_data_source_updated_times + 1 END;

# Return
MATCH (p:Person {metadata_data_source_name: 'sinafip'}) RETURN p

# RCND2023:
# Test: LOAD ORG OR PERSON
LOAD CSV WITH HEADERS FROM 'file:///csv/sinafip-scraper-rcnd2023/rcnd2023-page-2/1021349/1_PROPAGANDA_Y_PUBLICIDAD.csv' AS row
WITH row, split(row.`C.I/RUC`, '-') AS ruc
FOREACH(ignoreMe IN CASE WHEN ruc[0] =~ '800[0-9]{5}' THEN [1] ELSE [] END | MERGE (p:Organization {id: ruc[0]})
    SET p.name = row.`PERSONA FISICA / JURIDICA`,
    p.scheme = 'PY-RUC',
    p.metadata_data_source_name = 'sinafip',
    p.metadata_data_source_country ='PY',
    p.metadata_data_source_created_at = CASE WHEN p.metadata_data_source_created_at is NULL THEN datetime() ELSE p.metadata_data_source_created_at END,
    p.metadata_data_source_lastest_update = datetime(),
    p.metadata_data_source_updated_times = CASE WHEN p.metadata_data_source_updated_times IS NULL THEN 1 ELSE p.metadata_data_source_updated_times + 1 END
)
FOREACH(ignoreMe IN CASE WHEN NOT (ruc[0] =~ '(800([0-9]{5}))') THEN [1] ELSE [] END | MERGE (p:Person {id: ruc[0]})
    SET p.name = row.`PERSONA FISICA / JURIDICA`,
        p.scheme = 'PY-RUC',
        p.metadata_data_source_name = 'sinafip',
        p.metadata_data_source_country ='PY',
        p.metadata_data_source_created_at = CASE WHEN p.metadata_data_source_created_at is NULL THEN datetime() ELSE p.metadata_data_source_created_at END,
        p.metadata_data_source_lastest_update = datetime(),
        p.metadata_data_source_updated_times = CASE WHEN p.metadata_data_source_updated_times IS NULL THEN 1 ELSE p.metadata_data_source_updated_times + 1 END
)

# DIEV
# LOAD MAIN PAGES:
# Candidate:
LOAD CSV WITH HEADERS FROM 'file:///csv/sinafip-scraper-diev/main-pages/5A3/diev-main-page-1.csv' AS row
MERGE (p:Person {id: row.`Cédula`})
    SET p.name = row.`Nombre` + ' ' + row.`Apellido`,
    p.scheme = 'PY-RUC',
    p.metadata_data_source_name = 'sinafip',
    p.metadata_data_source_country ='PY',
    p.metadata_data_source_created_at = CASE WHEN p.metadata_data_source_created_at is NULL THEN datetime() ELSE p.metadata_data_source_created_at END,
    p.metadata_data_source_lastest_update = datetime(),
    p.metadata_data_source_updated_times = CASE WHEN p.metadata_data_source_updated_times IS NULL THEN 1 ELSE p.metadata_data_source_updated_times + 1 END;

# Load Spouse
LOAD CSV WITH HEADERS FROM 'file:///csv/sinafip-scraper-diev/main-pages/5A3/diev-main-page-1.csv' AS row
WITH row WHERE row.`Cédula Conyuge` IS NOT NULL
MERGE (p:Person {id: row.`Cédula Conyuge`})
    SET p.name = row.`Nombre Conyuge` + ' ' + row.`Apellido Conyuge`,
    p.scheme = 'PY-RUC',
    p.metadata_data_source_name = 'sinafip',
    p.metadata_data_source_country ='PY',
    p.metadata_data_source_created_at = CASE WHEN p.metadata_data_source_created_at is NULL THEN datetime() ELSE p.metadata_data_source_created_at END,
    p.metadata_data_source_lastest_update = datetime(),
    p.metadata_data_source_updated_times = CASE WHEN p.metadata_data_source_updated_times IS NULL THEN 1 ELSE p.metadata_data_source_updated_times + 1 END;

# Set relationship  IS_SPOUSE
LOAD CSV WITH HEADERS FROM 'file:///csv/sinafip-scraper-diev/main-pages/5A3/diev-main-page-1.csv' AS row
WITH row WHERE row.`Cédula Conyuge` <> ''
MATCH (p1:Person {id: row.`Cédula`})
MERGE (p2:Person {id: row.`Cédula Conyuge`})
MERGE (p1)-[r:IS_SPOUSE]->(p2);

# Get Persons with relationship IS_SPOUSE
MATCH (p1: Person)-[r:IS_SPOUSE]-(p2: Person) RETURN p1,r,p2

# Load Organization from DIEV report
LOAD CSV WITH HEADERS FROM "file:///csv/sinafip-scraper-diev/reports/566/1021349_candidato/1_Personas_Juridicas_en_las_que_el_la_candidato_a_es_accionista_socio_asociado.csv" AS row
WITH row, split(row.`RUC`, '-') AS ruc
WHERE (row.`% Porcentaje de participación en relación al capital` <> '0.00') AND (row.`% Porcentaje de participación en relación al capital` <> '0')
MERGE (o:Organization {id: CASE WHEN ruc[0] = 'DATOS PROTEGIDOS' THEN randomUUID() ELSE ruc[0] END})
    SET o.name = row.`Razón Social`,
    o.scheme = 'PY-RUC',
    o.metadata_data_source_name = 'sinafip',
    o.metadata_data_source_country ='PY',
    o.metadata_data_source_created_at = CASE WHEN o.metadata_data_source_created_at is NULL THEN datetime() ELSE o.metadata_data_source_created_at END,
    o.metadata_data_source_lastest_update = datetime(),
    o.metadata_data_source_updated_times = CASE WHEN o.metadata_data_source_updated_times IS NULL THEN 1 ELSE o.metadata_data_source_updated_times + 1 END;

# Create OWNs relationship with 
LOAD CSV WITH HEADERS FROM "file:///csv/sinafip-scraper-diev/reports/566/1021349_candidato/1_Personas_Juridicas_en_las_que_el_la_candidato_a_es_accionista_socio_asociado.csv" AS row
WITH row, split(row.`RUC`, '-') AS ruc
MATCH (o: Organization {id: ruc[0]})
MATCH (p: Person {id: row.`CI`})
MERGE (p)-[r:OWNS]->(o);

# Get OWN relationship
MATCH (p: Person)-[r:OWNS]->(o:Organization {metadata_data_source_name: 'sinafip'}) RETURN p,r,o

# Create OWNs with name
LOAD CSV WITH HEADERS FROM "file:///csv/sinafip-scraper-diev/reports/566/1021349_candidato/1_Personas_Juridicas_en_las_que_el_la_candidato_a_es_accionista_socio_asociado.csv" AS row
MATCH (o: Organization) WHERE (toLower(o.`name`) CONTAINS toLower(row.`Razón Social`) OR toLower(row.`Razón Social`) CONTAINS toLower(o.`name`))
MATCH (p: Person {id: row.`CI`})
MERGE (p)-[r:OWNS]->(o);