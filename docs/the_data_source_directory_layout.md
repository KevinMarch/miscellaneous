# The data source directory layout

Full data source directory path in a Unix filesystem-like structure
````
./data/:data_source_name:/:timestamp:/:sub_directory_1:/:sub_directory_2:/:disambiguator:/:resource_name.format:
````

Use lowercase for directory names. Replace any spaces with underscores.

Directories
* :data_source_name: Every datasource has name. Check out [How to choose a data source name?](./how_to_choose_a_data_source_name.md).
* :timestamp: Follow the date and time with the offset representation of [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601). Replace any non-numerical symbol.
* :sub_directory_1: Apply a hash function strategy on data_source_name. For example the subdirectory store of [OCDS Kingfisher Collect](https://github.com/open-contracting/kingfisher-collect/blob/main/kingfisher_scrapy/extensions/files_store.py#L87C1-L94C30).
* :sub_directory_2: Apply a hash function strategy on data_source_name. For example the subdirectory store of [OCDS Kingfisher Collect](https://github.com/open-contracting/kingfisher-collect/blob/main/kingfisher_scrapy/extensions/files_store.py#L87C1-L94C30).
* :disambiguator: if needed. For example "social security number".
* :resource_name.format: For example "ocid_prefixes.csv".
