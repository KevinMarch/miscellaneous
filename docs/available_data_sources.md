# Available data sources

* ocp_ocds_ocid_prefixes
* paraguay_ocds_dncp_records
* paraguay_instidea_beneficial_ownership
* paraguay_cgr_assets_declaration
* guatemala_guatecompras_ocds_records
* panama_dgcp_ocds_records
* peru_compras_ocds_releases
* peru_osce_ocds_api_records
* peru_osce_supplier_profile