# How to choose a data source name?

Based on the "[Choose a spider name](https://kingfisher-collect.readthedocs.io/en/latest/contributing/index.html#choose-a-spider-name)" article of [OCDS Kingfisher Collect documentation](https://kingfisher-collect.readthedocs.io/en/latest/index.html) by [Open Contracting Partnership](https://www.open-contracting.org/), published on [2024-01-24](https://github.com/open-contracting/kingfisher-collect/blob/b7765222f0a2b7468a7b1da6cef47ab2adf8ae5e/docs/contributing/index.rst#choose-a-spider-name).

Use lowercase and join the components below with underscores. Replace any spaces with underscores.

* Country name. Follow the english short name of [ISO 3166-1](https://en.wikipedia.org/wiki/ISO_3166-1). For example "paraguay", "panama", "guatemala" and "peru".
* Data standard short name, if needed. For example "ocds" is the short name of [Open Contracting Data Standard](https://standard.open-contracting.org/latest/en/primer/what/#what-is-the-ocds-and-why-use-it).
* System short name, if needed.
* Publisher short name, if needed.
* Resource name, if needed. For example "ocid_prefixes".
* Disambiguator, if needed. For example "historical".
* Access method, if needed. For example "bulk" or "api".
* Data standard format, if needed. For example "releases", "records", "release_packages" or "record_packages".